# zpe
A utility to ease the management of shell plugins (particularly Zsh). This is NOT a complete plugin manager, I recommend [znap](https://github.com/marlonrichert/zsh-snap/) if you are looking for one. This script just takes the edge off of manually managing them, if you don't want to deal with one of the full-fledged plugin managers.

## Usage
### Basic usage
  1. Source the script in your shell rc
  2. Add a list of the plugin repositories in `~/.config/zpe/repositories`. Use the full URL, either SSH or HTTP, comments can be added with a #.
  3. Run `zpe-clone` to pull the repositories
  4. Source the plugin in your shell rc using the `zpe-source` command. For each plugin, locate the source-able file (usually contains the word init, or has the .plugin.zsh extension). Add it to your shell rc using the following syntax: `zpe-source <plugin name>/<source-able file name>`. Make sure that this goes after sourcing the `zpe.sh` script.

### Extra commands
  * `zpe-pull` updates the repositories
  * `zpe-clean` removes any cloned repositories that are no longer in the `repositories` file
  * `zpe` opens the `repositories` file in $EDITOR

## Motivation
All of the other available plugin managers try to get way too fancy. I just need something to automatically clone git repositories, and make my config nice and portable. The script is intentionally highly extensible and feature-minimal.

## License
Copyright Armaan Bhojwani 2021, MIT License, see the LICENSE file for more information
